package br.com.alura.service;

import java.sql.SQLException;

import org.hibernate.exception.ConstraintViolationException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import br.com.alura.model.Usuario;
import br.com.alura.service.UsuarioService;
import io.quarkus.test.junit.QuarkusTest;
import jakarta.inject.Inject;

@QuarkusTest
public class UsuarioServiceTest {
	
	@Inject
	UsuarioService usuarioService;

	@Test
	public void criaNovoUsuarioComSucesso() {
		Usuario usuario = new Usuario();
		usuario.setCpf("655.484.547-01");
		usuario.setNome("Maria");
		usuario.setPassword("123456");
		usuario.setUsername("maria");
		
		Usuario usuarioCriado = usuarioService.criarUsuario(usuario);
		
		Assertions.assertTrue(usuarioCriado.getId() != null);
		Assertions.assertTrue(usuarioCriado.getCpf().equals(usuario.getCpf()));
		Assertions.assertTrue(usuarioCriado.getNome().equals(usuario.getNome()));
		Assertions.assertTrue(usuarioCriado.getUsername().equals(usuario.getUsername()));
		
		usuarioService.apagarUsuario(usuarioCriado);
	}
	
	@Test
	public void naoCriaNovoUsuarioPoisCPFeNulo() {
		Usuario usuario = new Usuario();
		// usuario.setCpf("655.484.547-01");
		usuario.setNome("Maria");
		usuario.setPassword("123456");
		usuario.setUsername("maria");
		
		Assertions.assertThrows(ConstraintViolationException.class, () -> {
			usuarioService.criarUsuario(usuario);
		});
	}
	
	@Test
	public void naoCriaNovoUsuarioPoisNOMEeNulo() {
		Usuario usuario = new Usuario();
		usuario.setCpf("655.484.547-01");
		// usuario.setNome("Maria");
		usuario.setPassword("123456");
		usuario.setUsername("maria");
		
		Assertions.assertThrows(ConstraintViolationException.class, () -> {
			usuarioService.criarUsuario(usuario);
		});
	}
	
	@Test
	public void naoCriaNovoUsuarioPoisPASSWORDeNulo() {
		Usuario usuario = new Usuario();
		usuario.setCpf("655.484.547-01");
		usuario.setNome("Maria");
		// usuario.setPassword("123456");
		usuario.setUsername("maria");
		
		Assertions.assertThrows(NullPointerException.class, () -> {
			usuarioService.criarUsuario(usuario);
		});
	}
	
	@Test
	public void naoCriaNovoUsuarioPoisUSERNAMEeNulo() {
		Usuario usuario = new Usuario();
		usuario.setCpf("655.484.547-01");
		usuario.setNome("Maria");
		usuario.setPassword("123456");
		// usuario.setUsername("maria");
		
		Assertions.assertThrows(NullPointerException.class, () -> {
			usuarioService.criarUsuario(usuario);
		});
	}
}
