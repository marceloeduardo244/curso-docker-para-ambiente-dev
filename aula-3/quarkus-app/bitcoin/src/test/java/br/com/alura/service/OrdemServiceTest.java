package br.com.alura.service;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import br.com.alura.model.Ordem;
import br.com.alura.model.Usuario;
import io.quarkus.test.junit.QuarkusTest;
import jakarta.inject.Inject;
import jakarta.ws.rs.core.SecurityContext;

@QuarkusTest
public class OrdemServiceTest {
	
	@Inject
	UsuarioService usuarioService;
	
	@Inject
	OrdemService ordemService;

	@Test
	public void criaOrdemComSucesso() {
		Usuario usuario = new Usuario();
		usuario.setCpf("655.484.547-01");
		usuario.setNome("Maria");
		usuario.setPassword("123456");
		usuario.setUsername("maria");
		
		Usuario usuarioCriado = usuarioService.criarUsuario(usuario);
		
		Ordem ordem = new Ordem();
		ordem.setPreco(55.55);
		ordem.setTipo("COMPRA");
		ordem.setUserId(usuarioCriado.getId());
		
		Ordem ordemCriada = ordemService.criarOrdem(ordem);
		
		Assertions.assertTrue(ordemCriada.getId() != null);
		Assertions.assertTrue(ordemCriada.getPreco().equals(ordem.getPreco()));
		Assertions.assertTrue(ordemCriada.getTipo().equals(ordem.getTipo()));
		Assertions.assertTrue(ordemCriada.getUserId().equals(ordem.getUserId()));
		
		ordemService.apagaOrdem(ordemCriada);
		usuarioService.apagarUsuario(usuarioCriado);
	}
}
