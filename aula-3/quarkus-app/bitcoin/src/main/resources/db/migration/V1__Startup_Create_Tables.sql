CREATE TABLE Usuario (
    id SERIAL PRIMARY KEY,
    nome VARCHAR(50) NOT NULL,
    cpf VARCHAR(14) NOT NULL,
    username VARCHAR(50) NOT NULL,
    password VARCHAR(100) NOT NULL
);

CREATE TABLE Ordem (
    id SERIAL PRIMARY KEY,
    preco NUMERIC(6,2) NOT NULL,
    tipo VARCHAR(20) NOT NULL,
    data TIMESTAMP NOT NULL,
    status VARCHAR(30) NOT NULL,
    user_id INT NOT NULL,
    FOREIGN KEY (user_id) REFERENCES Usuario (id)
);