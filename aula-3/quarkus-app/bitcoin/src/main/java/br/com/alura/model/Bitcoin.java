package br.com.alura.model;

import java.time.LocalDate;

import jakarta.json.bind.annotation.JsonbDateFormat;
import jakarta.json.bind.annotation.JsonbProperty;

public class Bitcoin {

    @JsonbProperty("id")
    private Long id;

    @JsonbProperty("preco")
    private Double preco;

    @JsonbProperty("tipo")
    private String tipo;

    @JsonbDateFormat("yyyy-MM-dd")
    @JsonbProperty("data")
    private LocalDate data;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Double getPreco() {
		return preco;
	}

	public void setPreco(Double preco) {
		this.preco = preco;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public LocalDate getData() {
		return data;
	}

	public void setData(LocalDate data) {
		this.data = data;
	}

}
