package br.com.alura.service;

import java.util.List;

import br.com.alura.model.Usuario;
import br.com.alura.repository.UsuarioRepository;
import io.quarkus.elytron.security.common.BcryptUtil;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.transaction.Transactional;

@ApplicationScoped
public class UsuarioService {
	@Inject
	UsuarioRepository usuarioRepository;
	
	@Transactional
	public Usuario criarUsuario(Usuario usuario) {
		adicionarCredenciais(usuario);
		usuarioRepository.persist(usuario);
		
		return usuario;
	}
	
	public static void adicionarCredenciais(Usuario usuario) {
		usuario.setPassword(BcryptUtil.bcryptHash(usuario.getPassword()));
		usuario.setRole(validarUsername(usuario.getUsername()));
	}
	
	public static String validarUsername(String username) {
		if (username.equals("alura")) {
			return "admin";
		}
		return "user";
	}

	public List<Usuario> getList() {
		return usuarioRepository.listAll();
	}
	
	@Transactional
	public void apagarUsuarioPeloId(Long id) {
		usuarioRepository.deleteById(id);
	}
	
	@Transactional 
	public void apagarUsuario(Usuario usuario) {
		usuarioRepository.delete(usuario);
	}
}
