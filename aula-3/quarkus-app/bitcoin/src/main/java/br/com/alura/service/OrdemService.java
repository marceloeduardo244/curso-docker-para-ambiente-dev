package br.com.alura.service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import br.com.alura.model.Ordem;
import br.com.alura.model.Usuario;
import br.com.alura.repository.OrdemRepository;
import br.com.alura.repository.UsuarioRepository;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.transaction.Transactional;
import jakarta.ws.rs.core.SecurityContext;

@ApplicationScoped
public class OrdemService {
	@Inject
	OrdemRepository ordemRepository;
	
	@Inject
	UsuarioRepository usuarioRepository;
	
	@Transactional
	public Ordem criarOrdem(Ordem ordem) {
		
		ordem.setData(LocalDateTime.now());
		ordem.setStatus("ENVIADA");
		ordemRepository.persist(ordem);
		
		return ordem;
	}

	public List<Ordem> getList() {
		return ordemRepository.listAll();
	}
	
	@Transactional
	public void apagaOrdem(Ordem ordem) {
		ordemRepository.delete(ordem);
	}
}
