![image](https://cdn-hieaj.nitrocdn.com/XZigabkLPVfJOMZfzYheLxDVSQbARBXx/assets/images/optimized/rev-902bd1c/wp-content/uploads/2021/06/quarkus-500x551.jpg)


# 🚀 Projeto Quarkus API

Bem-vindo(a). Este é o Projeto Quarkus API!

O objetivo deste projeto é relaizar uma implementação simples de uma API Quarkus, consumindo api externa, com testes de integração, pipelines e tudo isso rodando em uma stack docker-compose.

# 🧠 Contexto

Tecnologias utilizadas neste produto:
- Java 11
- JPA/Hibernate
- Flyway - migrations
- Quarkus v3
- PostgreSQL
- Pgadmin
- JUnit para os testes de integração
- Autenticação simples com Basic-Auth
- Pipeline End to End
    - Na pipeline temos 3 jobs
        - run_migrations para validar se as migrations estão saudáveis e válidas
        - test para rodar todos os testes de integração da aplicação
        - build para gerar o arquivo .jar
-  Docker & Docker-compose

# Rotas da API
GET -  /bitcoins - retorna os dados de transações da api externa
GET -  /usuarios - retorna lista de usuarios
POST - /usuarios - cria novo usuario
GET -  /ordens   - retorna lista de ordens
POST - /ordens   - cria nova ordem

- link para download da collection feita no postman que pode apox feito o download pode ser importada no postman ou insomnia (https://gitlab.com/marceloeduardo244/quarkus-project-bitcoin/-/blob/main/docs/QUARKUS-API.postman_collection.json)


## 📋 Instruções para subir a aplicação utilizando Docker-compose

É necessário ter o Docker e Docker-compose instalado e rodando na sua maquina!

- Faça clone do projeto
- Na raiz do projeto execute o comando abaixo:
- docker-compose up --build -d

Made with 💜 at OliveiraDev