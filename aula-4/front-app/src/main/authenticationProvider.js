import React from 'react'

import AuthService from '../app/service/authService'
import jwt from 'jsonwebtoken'

export const AuthContext = React.createContext()
export const AuthConsumer = AuthContext.Consumer;

const AuthProvider = AuthContext.Provider;

class ProvedorAutenticacao extends React.Component{

    state = {
        autheticatedUser: null,
        isAuthenticated: false,
        isLoading: true
    }

    startSession = (tokenDTO) => {
        const token = tokenDTO.token
        const claims = jwt.decode(token)
        const usuario = {
            id: claims.userid,
            name: claims.name
        }
        
        AuthService.doLogin(usuario, token);
        this.setState({ isAuthenticated: true, autheticatedUser: usuario })
    }

    finishSession = () => {
        AuthService.removeAuthenticatedUser();
        this.setState({ isAuthenticated: false, autheticatedUser: null})
    }

    async componentDidMount(){
        const isAuthenticated = AuthService.isAuthenticatedUser()
        if(isAuthenticated){
            const user = await AuthService.refreshSession()
            this.setState({
                isAuthenticated: true,
                autheticatedUser: user,
                isLoading: false
            })
        }else{
            this.setState( previousState => {
                return {
                    ...previousState,
                    isLoading: false
                }
            })
        }
    }

    render(){

        if(this.state.isLoading){
            return null;
        }

        const contexto = {
            autheticatedUser: this.state.autheticatedUser,
            isAuthenticated: this.state.isAuthenticated,
            startSession: this.startSession,
            finishSession: this.finishSession
        }

        return(
            <AuthProvider value={contexto} >
                {this.props.children}
            </AuthProvider>
        )
    }
}

export default ProvedorAutenticacao;