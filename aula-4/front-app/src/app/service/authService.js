import LocalStorageService from './localstorageService'

import jwt from 'jsonwebtoken'
import ApiService from '../apiservice'

export const LOGGED_USER = '_LOGGED_USER'
export const TOKEN = 'access_token'

export default class AuthService {

    static isAuthenticatedUser(){
        const token = LocalStorageService.adquireItem(TOKEN)
        if(!token){
            return false;
        }
        const decodedToken = jwt.decode(token)
        const expiration = decodedToken.exp

        const isTokenInvalido = Date.now() >= (expiration * 1000)

        return !isTokenInvalido;
    }

    static removeAuthenticatedUser(){
        LocalStorageService.deleteItem(LOGGED_USER)
        LocalStorageService.deleteItem(TOKEN);
    }

    static doLogin(usuario, token){
        LocalStorageService.addItem(LOGGED_USER, usuario)
        LocalStorageService.addItem(TOKEN, token);
        ApiService.registrarToken(token)
    }

    static getAuthenticatedUser(){
        return LocalStorageService.adquireItem(LOGGED_USER);
    }

    static refreshSession(){
        const token  = LocalStorageService.adquireItem(TOKEN)
        const usuario = AuthService.getAuthenticatedUser()
        AuthService.doLogin(usuario, token)
        return usuario;
    }

}