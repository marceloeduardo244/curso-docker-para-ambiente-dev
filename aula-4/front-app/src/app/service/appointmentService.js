import ApiService from '../apiservice'

import ValidationError from '../exception/validationError'

export default class AppointmentService extends ApiService {

    constructor(){
        super('/api/appointment')
    }

    getMonthList(){
        return  [
            { label: 'Selecione...', value: '' },
            { label: 'Janeiro', value: 1 },
            { label: 'Fevereiro', value: 2 },
            { label: 'Março', value: 3 },
            { label: 'Abril', value: 4 },
            { label: 'Maio', value: 5 },
            { label: 'Junho', value: 6 },
            { label: 'Julho', value: 7 },
            { label: 'Agosto', value: 8 },
            { label: 'Setembro', value: 9 },
            { label: 'Outubro', value: 10 },
            { label: 'Novembro', value: 11 },
            { label: 'Dezembro', value: 12 },
        ]
    }

    getTypeList(){
        return  [
            { label: 'Selecione...', value: '' },
            { label: 'Despesa' , value : 'DEBIT' },
            { label: 'Receita' , value : 'RECEIPT' }
        ]

    }

    getById(id){
        return this.get(`/${id}`);
    }

    changeStatus(id, status){
        return this.put(`/${id}/update-status`, { status })
    }

    validate(appointment){
        const erros = [];

        if(!appointment.year){
            erros.push("Informe o Ano.")
        }

        if(!appointment.month){
            erros.push("Informe o Mês.")
        }

        if(!appointment.description){
            erros.push("Informe a Descrição.")
        }

        if(!appointment.value){
            erros.push("Informe o Valor.")
        }

        if(!appointment.type){
            erros.push("Informe o Tipo.")
        }

        if(erros && erros.length > 0){
            throw new ValidationError(erros);
        }
    }

    save(appointment){
        return this.post('/', appointment);
    }

    update(appointment){
        return this.put(`/${appointment.id}`, appointment);
    }

    doSearch(appointmentFilter){
        let params = `?year=${appointmentFilter.year}`

        if(appointmentFilter.month){
            params = `${params}&month=${appointmentFilter.month}`
        }

        if(appointmentFilter.type){
            params = `${params}&type=${appointmentFilter.type}`
        }

        if(appointmentFilter.status){
            params = `${params}&status=${appointmentFilter.status}`
        }

        if(appointmentFilter.user_id){
            params = `${params}&user_id=${appointmentFilter.user_id}`
        }

        if(appointmentFilter.description){
            params = `${params}&description=${appointmentFilter.description}`
        }

        return this.get(params);
    }

    remove(id){
        return this.delete(`/${id}`)
    }
}