import React from 'react'

import NavbarItem from './navbarItem'
import { AuthConsumer } from '../main/authenticationProvider'

function Navbar(props){
    return (
        <div className="navbar navbar-expand-lg fixed-top navbar-dark bg-primary">
        <div className="container">
          <a href="/home" className="navbar-brand">Spring Finance</a>
          <button className="navbar-toggler" type="button" 
                  data-toggle="collapse" data-target="#navbarResponsive" 
                  aria-controls="navbarResponsive" aria-expanded="false" 
                  aria-label="Toggle navigation">
            <span className="navbar-toggler-icon"></span>
          </button>
          <div className="collapse navbar-collapse" id="navbarResponsive">
            <ul className="navbar-nav">
                <NavbarItem render={props.isAuthenticatedUser} href="/home" label="Home" />
                <NavbarItem render={props.isAuthenticatedUser} href="/create-user" label="Usuários" />
                <NavbarItem render={props.isAuthenticatedUser} href="/get-appointment" label="Lançamentos" />
                <NavbarItem render={props.isAuthenticatedUser} onClick={props.desdoLogin} href="/login" label="Sair" />
            </ul>
            </div>
        </div>
      </div>
    )
}

export default () => (
  <AuthConsumer>
    {(context) => (
        <Navbar isAuthenticatedUser={context.isAuthenticated} desdoLogin={context.finishSession} />
    )}
  </AuthConsumer>
)