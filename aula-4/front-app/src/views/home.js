import React from 'react'

import UsuarioService from '../app/service/usuarioService'
import { AuthContext } from '../main/authenticationProvider'

class Home extends React.Component{

    state = {
        balance: 0,
        loggedUserName: ''
    }

    constructor(){
        super()
        this.usuarioService = new UsuarioService();
    }

    componentDidMount(){
        const loggedUser = this.context.autheticatedUser

        this.setState({ loggedUserName: loggedUser.name})

        this.usuarioService
            .getBalanceById(loggedUser.id)
            .then( response => {
                this.setState({ balance: response.data})
            }).catch(error => {
                console.error(error.response)
            });
    }

    render(){
        return (
            <div className="jumbotron">
                <h1 className="display-3">Bem vindo {this.state.loggedUserName}!</h1>
                <p className="lead">Esse é seu sistema de finanças.</p>
                <p className="lead">Seu saldo para o mês atual é de 
                <h3 className={this.state.balance > 0 ? 'text-success' : 'text-danger'}>R$ {this.state.balance}</h3> 
                </p>
                <hr className="my-4" />
                <p>E essa é sua área administrativa, utilize um dos menus ou botões abaixo para navegar pelo sistema.</p>
                <p className="lead">
                    <a className="btn btn-primary btn-lg" 
                    href="/create-user" 
                    role="button"><i className="pi pi-users"></i>  
                     Adicionar Usuário
                    </a>
                    <a className="btn btn-danger btn-lg" 
                    href="/create-appointment" 
                    role="button"><i className="pi pi-money-bill"></i>  
                     Adicionar Lançamento
                    </a>
                </p>
            </div>
        )
    }
}

Home.contextType = AuthContext;

export default Home