import React from 'react'
import { withRouter } from 'react-router-dom'

import Card from '../../components/card'
import FormGroup from '../../components/form-group'
import SelectMenu from '../../components/selectMenu'
import LancamentosTable from './tableOfAppointments'
import AppointmentService from '../../app/service/appointmentService'
import LocalStorageService from '../../app/service/localstorageService'

import * as messages from '../../components/toastr'

import {Dialog} from 'primereact/dialog';
import {Button} from 'primereact/button';



class ConsultaLancamentos extends React.Component {

    state = {
        year: '',
        month: '',
        type: '',
        description: '',
        showConfirmDialog: false,
        deleteAppointment: {},
        appointments : []
    }

    constructor(){
        super();
        this.service = new AppointmentService();
    }

    buscar = () => {
        if(!this.state.year){
            messages.errorMessage('O preenchimento do campo Ano é obrigatório.')
            return false;
        }

        const loggedUser = LocalStorageService.adquireItem('_LOGGED_USER');

        console.log(loggedUser)
        const appointmentFilter = {
            year: this.state.year,
            month: this.state.month,
            type: this.state.type,
            description: this.state.description,
            user_id: loggedUser.id
        }

        this.service
            .doSearch(appointmentFilter)
            .then( resposta => {
                const listOfAppointments = resposta.data;
                
                if(listOfAppointments.length < 1){
                    messages.alertMessage("Nenhum resultado encontrado.");
                }
                this.setState({ appointments: listOfAppointments })
            }).catch( error => {
                console.log(error)
            })
    }

    edit = (id) => {
        this.props.history.push(`/create-appointment/${id}`)
    }

    openConfirmation = (appointment) => {
        this.setState({ showConfirmDialog : true, deleteAppointment: appointment  })
    }

    cancelDelecao = () => {
        this.setState({ showConfirmDialog : false, deleteAppointment: {}  })
    }

    removeAppointment = () => {
        this.service
            .remove(this.state.deleteAppointment.id)
            .then(response => {
                const appointments = this.state.appointments;
                const index = appointments.indexOf(this.state.lancamentodelete)
                appointments.splice(index, 1);
                this.setState( { appointments: appointments, showConfirmDialog: false } )
                messages.successMessage('Lançamento deletado com sucesso!')
            }).catch(error => {
                messages.errorMessage('Ocorreu um erro ao tentar delete o Lançamento')
            })
    }

    preparaFormularioCadastro = () => {
        this.props.history.push('/create-appointment')
    }

    changeStatus = (appointment, status) => {
        this.service
            .changeStatus(appointment.id, status)
            .then( response => {
                const appointments = this.state.appointments;
                const index = appointments.indexOf(appointment);
                if(index !== -1){
                    appointment['status'] = status;
                    appointments[index] = appointment
                    this.setState({appointment});
                }
                messages.successMessage("Status atualizado com sucesso!")
            })
    }

    render(){
        const months = this.service.getMonthList();
        const types = this.service.getTypeList();

        const confirmDialogFooter = (
            <div>
                <Button label="Confirmar" icon="pi pi-check" onClick={this.removeAppointment} />
                <Button label="cancel" icon="pi pi-times" onClick={this.cancelDelecao} 
                        className="p-button-secondary" />
            </div>
        );

        return (
            <Card title="Consulta Lançamentos">
                <div className="row">
                    <div className="col-md-6">
                        <div className="bs-component">
                            <FormGroup htmlFor="inputAno" label="Ano: *">
                                <input type="text" 
                                       className="form-control" 
                                       id="inputAno" 
                                       value={this.state.year}
                                       onChange={e => this.setState({year: e.target.value})}
                                       placeholder="Digite o Ano" />
                            </FormGroup>

                            <FormGroup htmlFor="inputMes" label="Mês: ">
                                <SelectMenu id="inputMes" 
                                            value={this.state.month}
                                            onChange={e => this.setState({ month: e.target.value })}
                                            className="form-control" 
                                            lista={months} />
                            </FormGroup>

                            <FormGroup htmlFor="inputDesc" label="Descrição: ">
                                <input type="text" 
                                       className="form-control" 
                                       id="inputDesc" 
                                       value={this.state.description}
                                       onChange={e => this.setState({description: e.target.value})}
                                       placeholder="Digite a descrição" />
                            </FormGroup>

                            <FormGroup htmlFor="inputTipo" label="Tipo Lançamento: ">
                                <SelectMenu id="inputTipo" 
                                            value={this.state.type}
                                            onChange={e => this.setState({ type: e.target.value })}
                                            className="form-control" 
                                            lista={types} />
                            </FormGroup>

                            <button onClick={this.buscar} 
                                    type="button" 
                                    className="btn btn-success">
                                    <i className="pi pi-search"></i> Buscar
                            </button>
                            <button onClick={this.preparaFormularioCadastro} 
                                    type="button" 
                                    className="btn btn-danger">
                                    <i className="pi pi-plus"></i> Cadastrar
                            </button>

                        </div>
                        
                    </div>
                </div>   
                <br/ >
                <div className="row">
                    <div className="col-md-12">
                        <div className="bs-component">
                            <LancamentosTable appointments={this.state.appointments} 
                                              deleteAction={this.openConfirmation}
                                              editAction={this.edit}
                                              changeStatus={this.changeStatus} />
                        </div>
                    </div>  
                </div> 
                <div>
                    <Dialog header="Confirmação" 
                            visible={this.state.showConfirmDialog} 
                            style={{width: '50vw'}}
                            footer={confirmDialogFooter} 
                            modal={true} 
                            onHide={() => this.setState({showConfirmDialog: false})}>
                        Confirma a exclusão deste Lançamento?
                    </Dialog>
                </div>           
            </Card>

        )
    }
}

export default withRouter(ConsultaLancamentos);