import React from 'react'
import currencyFormatter from 'currency-formatter'

export default props => {

    const rows = props.appointments.map( appointment => {
        return (
            <tr key={appointment.id}>
                <td>{appointment.description}</td>
                <td>{ currencyFormatter.format(appointment.value, { locale: 'pt-BR'}) }</td>
                <td>{appointment.type}</td>
                <td>{appointment.month}</td>
                <td>{appointment.status}</td>
                <td>
                    <button className="btn btn-success" title="Efetivar"
                            disabled={ appointment.status !== 'OPEN' }
                            onClick={e => props.changeStatus(appointment, 'CLOSED')} 
                            type="button">
                            <i className="pi pi-check"></i>
                    </button>
                    <button className="btn btn-warning"  title="cancel"
                            disabled={ appointment.status !== 'OPEN' }
                            onClick={e => props.changeStatus(appointment, 'CANCELED')} 
                            type="button">
                            <i className="pi pi-times"></i>
                    </button>
                    <button type="button"   title="edit"
                            className="btn btn-primary"
                            onClick={e => props.editAction(appointment.id)}>
                            <i className="pi pi-pencil"></i>
                    </button>
                    <button type="button"  title="Excluir"
                            className="btn btn-danger" 
                            onClick={ e => props.deleteAction(appointment)}>
                            <i className="pi pi-trash"></i>
                    </button>
                </td>
            </tr>
        )
    } )

    return (
        <table className="table table-hover">
            <thead>
                <tr>
                    <th scope="col">Descrição</th>
                    <th scope="col">Valor</th>
                    <th scope="col">Tipo</th>
                    <th scope="col">Mês</th>
                    <th scope="col">Situação</th>
                    <th scope="col">Ações</th>
                </tr>
            </thead>
            <tbody>
                {rows}
            </tbody>
        </table>
    )
}

