![image](https://img-c.udemycdn.com/course/750x422/2777074_6d96_11.jpg)


# 🚀 Spring Finance

Bem-vindo(a). Este é o Spring Finance!

O objetivo desta aplicação é simular um serviço financeiro, havendo clientes, transações e dados da conta.

# 🧠 Contexto

Tecnologias utilizadas neste projeto:
-  Java v8 (v1.8)
-  Spring-Boot v2.7
-  Flyway (para as migrations)
-  JPA / Hibernate para a rotinas de banco de dados
-  Postgres
-  Pgadmin
-  Docker
-  Toekn JWT e Cors
-  JUnit v5 para os testes de integração
-  Kubernetes
-  Pipeline End to End
    - Na pipeline temos 4 jobs
        - run_migrations para validar se as migrations estão saudáveis e válidas
        - build para gerar o arquivo .jar da aplicação
        - test para rodar todos os testes de intregração da app
        - deploy para fazer o build e atualizar a imagem no docker-hub

## 📋 Instruções para subir a aplicação utilizando Docker-compose

É necessário ter o Docker e Docker-compose instalado e rodando na sua maquina!

- Faça clone do projeto
- Na raiz do projeto execute os comandos abaixo:
- docker-compose pull
- docker-compose up -d
- Vc terá 3 novos container rodando na sua maquina.
- A API rodando por padrão na porta localhost:8080, do Postgres na porta localhost:5432 e o pgadmin na porta localhost:80.
- Abaixo nesta doc tem a documentação das rotas.

## 📋 Instruções para subir a aplicação utilizando o Kubernetes

É necessário ter o Docker e Docker-compose e Kubernets instalado e rodando na sua maquina!

- Faça clone do projeto
- Na raiz do projeto execute os comandos abaixo:
- docker-compose pull
- cd devops/kubernetes
- kubectl apply -f ./golang-go-finance
- Vc terá 6 novos container rodando na sua maquina.
- A API rodando por padrão na porta localhost:30002, o Postgres poderá ser acessado via pgadmin na porta localhost:30003
- Abaixo nesta doc tem a documentação das rotas.

## ✔️ Rotas da API

Alem da breve descrição abaixo sobre as rotas tbm há um link no fim para download da collection que poderá ser importada no seu postman ou insomnia:

- GET    /isAlive
- POST   /api/user
- POST   /api/user/auth
- GET    /api/user/{id}/balance
- POST   /api/appointment
- GET    /api/appointment/{id}
- PUT    /api/appointment/{id}/update-status
- PUT    /api/appointment/{id}
- GET    /api/appointment?user_id={id}
- DELETE /api/appointment/{id}

- link para download da collection feita no postman que pode apox feito o download pode ser importada no postman ou insomnia (https://gitlab.com/marceloeduardo244/spring-finance/-/blob/main/docs/SPRING-FINANCE.postman_collection.json) 

# Frontend da Aplicação
- https://gitlab.com/marceloeduardo244/spring-finance-web

# Integração completa da aplicação
- https://gitlab.com/marceloeduardo244/spring-finance-integrado

Made with 💜 at OliveiraDev