package com.oliveiradev.springfinance.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.oliveiradev.springfinance.model.entity.User;
import com.oliveiradev.springfinance.model.repository.UserRepository;

@Service
public class SecurityUserDetailService implements UserDetailsService {
	
	@Autowired
	private UserRepository userRepository;
	
	@Override
	public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
		User foundedUser = userRepository
				.findByEmail(email)
				.orElseThrow(() -> new UsernameNotFoundException("Email not found."));
		
		return org.springframework.security.core.userdetails.User.builder()
				.username(foundedUser.getEmail())
				.password(foundedUser.getPassword())
				.roles("USER")
				.build();
	}

}
