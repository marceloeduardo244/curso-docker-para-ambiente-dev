package com.oliveiradev.springfinance.api.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UtilController {
    @GetMapping("/isAlive")
    public String sayHello() {
        return "The Spring Finance is Alive!";
    }
}
