package com.oliveiradev.springfinance.api.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.oliveiradev.springfinance.exceptions.ApplicationPersonalizatedRulesException;

import com.oliveiradev.springfinance.api.dto.AppointmentDTO;
import com.oliveiradev.springfinance.api.dto.UpdateStatusDTO;
import com.oliveiradev.springfinance.model.entity.Appointment;
import com.oliveiradev.springfinance.model.entity.User;
import com.oliveiradev.springfinance.model.enums.AppointmentStatus;
import com.oliveiradev.springfinance.model.enums.AppointmentType;
import com.oliveiradev.springfinance.service.AppointmentService;
import com.oliveiradev.springfinance.service.UserService;

@RestController
@RequestMapping("/api/appointment")
public class AppointmentController {
	
	@Autowired
	AppointmentService appointmentService;
	
	@Autowired
	UserService userService;
	
	@GetMapping
	public ResponseEntity getListofAppointments(
			@RequestParam(value ="description" , required = false) String description,
			@RequestParam(value = "month", required = false) Integer month,
			@RequestParam(value = "year", required = false) Integer year,
			@RequestParam(value = "user_id") Long idUser
			) {
		
		Appointment appointmentFilter = new Appointment();
		appointmentFilter.setDescription(description);
		appointmentFilter.setMonth(month);
		appointmentFilter.setYear(year);
		
		Optional<User> user = userService.getUserById(idUser);
		if(!user.isPresent()) {
			return ResponseEntity.badRequest().body("Cannot be posible execute user search. User not found using the id especifield.");
		}else {
			appointmentFilter.setUser(user.get());
		}
		
		List<Appointment> appointments = appointmentService.getListAppointments(appointmentFilter);
		return ResponseEntity.ok(appointments);
	}
	
	@GetMapping("{id}")
	public ResponseEntity getAppointmentById( @PathVariable("id") Long id ) {
		return appointmentService.getAppointmentById(id)
					.map( appointment -> new ResponseEntity(converter(appointment), HttpStatus.OK) )
					.orElseGet( () -> new ResponseEntity(HttpStatus.NOT_FOUND) );
	}
	
	@PostMapping
	public ResponseEntity<Object> save(@RequestBody AppointmentDTO dto) {
		
		Appointment appointment = converter(dto);
		
		try {
			Appointment createdAppointment = appointmentService.createAppointment(appointment);
			return new ResponseEntity<Object>(createdAppointment, HttpStatus.CREATED);
		} catch (ApplicationPersonalizatedRulesException e) {
			return ResponseEntity.badRequest().body(e.getMessage());
		} catch (Exception e) {
			return ResponseEntity.internalServerError().body(e.getMessage());
		}
	}
	
	@PutMapping("{id}")
	public ResponseEntity update( @PathVariable("id") Long id, @RequestBody AppointmentDTO dto ) {
		return appointmentService.getAppointmentById(id).map( entity -> {
			try {
				Appointment appointment = converter(dto);
				appointment.setId(id);
				appointmentService.updateAppointment(appointment);
				return ResponseEntity.ok(appointment);
			} catch (Exception e) {
				return ResponseEntity.badRequest().body(e.getMessage());
			}
		}).orElseGet( () -> 
		new ResponseEntity("Appointment not found on database.", HttpStatus.BAD_REQUEST) );
	}
	
	@PutMapping("{id}/update-status")
	public ResponseEntity updateStatus( @PathVariable("id") Long id , @RequestBody UpdateStatusDTO dto ) {
		return appointmentService.getAppointmentById(id).map( entity -> {
			AppointmentStatus selectedStatus = AppointmentStatus.valueOf(dto.getStatus().toString());
			
			if(selectedStatus == null) {
				return ResponseEntity.badRequest().body("Cannot be possible to change the status of appointment, send a valid status.");
			}
			
			try {
				entity.setStatus(selectedStatus);
				appointmentService.updateStatus(entity, selectedStatus);
				return ResponseEntity.ok(entity);
			}catch (Exception e) {
				return ResponseEntity.badRequest().body(e.getMessage());
			}
		
		}).orElseGet( () ->
		new ResponseEntity("Appointment not found on database.", HttpStatus.BAD_REQUEST) );
	}
	
	@DeleteMapping("{id}")
	public ResponseEntity deletar( @PathVariable("id") Long id ) {
		return appointmentService.getAppointmentById(id).map( entity -> {
			appointmentService.deleteAppointment(entity);
			return new ResponseEntity( HttpStatus.NO_CONTENT );
		}).orElseGet( () -> 
			new ResponseEntity("Appointment not found on database.", HttpStatus.BAD_REQUEST) );
	}
	
	private AppointmentDTO converter(Appointment apt) {
		AppointmentDTO appointmentDTO = new AppointmentDTO();
		
		if (apt.getId() != null) {
			appointmentDTO.setId(apt.getId());
		}
		appointmentDTO.setDescription(apt.getDescription());
		appointmentDTO.setMonth(apt.getMonth());
		appointmentDTO.setStatus(apt.getStatus());
		appointmentDTO.setType(apt.getType());
		appointmentDTO.setValue(apt.getValue());
		appointmentDTO.setYear(apt.getYear());
		appointmentDTO.setDescription(apt.getDescription());
		appointmentDTO.setUser(apt.getUser());
		
		return appointmentDTO;			
	}
	
	private Appointment converter(AppointmentDTO dto) {
		Appointment appointment = new Appointment();
		
		appointment.setId(dto.getId());
		appointment.setDescription(dto.getDescription());
		appointment.setMonth(dto.getMonth());
		appointment.setStatus(dto.getStatus());
		appointment.setYear(dto.getYear());
		appointment.setDescription(dto.getDescription());
		appointment.setValue(dto.getValue());
		
		User user = userService.getUserById(dto.getUser().getId()).orElseThrow(() -> new ApplicationPersonalizatedRulesException("User not found using the provided Id"));
		
		appointment.setUser(user);
		
		if(dto.getType() != null) {
			appointment.setType(AppointmentType.valueOf(dto.getType().toString()));
		}
		
		if(dto.getStatus() != null) {
			appointment.setStatus(AppointmentStatus.valueOf(dto.getStatus().toString()));
		}
		
		return appointment;			
	}
	
}
