package com.oliveiradev.springfinance.exceptions;

public class AuthenticationErrorException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public AuthenticationErrorException(String message) {
		super(message);
	}

}
