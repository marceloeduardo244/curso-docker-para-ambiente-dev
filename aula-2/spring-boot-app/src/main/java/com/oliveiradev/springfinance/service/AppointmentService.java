package com.oliveiradev.springfinance.service;

import java.util.Optional;
import java.math.BigDecimal;
import java.util.List;

import com.oliveiradev.springfinance.model.entity.Appointment;
import com.oliveiradev.springfinance.model.enums.AppointmentStatus;

public interface AppointmentService {

	Appointment createAppointment(Appointment appointment);
	
	Appointment updateAppointment(Appointment appointment);
	
	void deleteAppointment(Appointment appointment);
	
	List<Appointment> getListAppointments(Appointment appointmentFilter);
	
	void updateStatus(Appointment appointment, AppointmentStatus status);
	
	void validateAppointment(Appointment appointment);

	Optional<Appointment> getAppointmentById(Long id);
	
	BigDecimal getBalanceByUserId(Long id);
	
	void deleteByDescription(String description);
		
}
