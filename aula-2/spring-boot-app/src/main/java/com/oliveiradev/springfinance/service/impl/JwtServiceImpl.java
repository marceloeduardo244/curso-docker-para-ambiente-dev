package com.oliveiradev.springfinance.service.impl;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.oliveiradev.springfinance.model.entity.User;
import com.oliveiradev.springfinance.service.JwtService;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@Service
public class JwtServiceImpl implements JwtService {
	
	@Value("${jwt.expiration}")
	private String expiration;
	
	@Value("${jwt.sign-key}")
	private String signKey;

	@Override
	public String generateToken(User user) {
		long exp = Long.valueOf(expiration);
		LocalDateTime dateHourExpiration = LocalDateTime.now().plusMinutes(exp);
		Instant instant = dateHourExpiration.atZone( ZoneId.systemDefault() ).toInstant();
		java.util.Date date = Date.from(instant);
		
		String expirationHourToken = dateHourExpiration.toLocalTime()
				.format(DateTimeFormatter.ofPattern("HH:mm"));
		
		String token = Jwts
							.builder()
							.setExpiration(date)
							.setSubject(user.getEmail())
							.claim("userid", user.getId())
							.claim("name", user.getName())
							.claim("expirationHour", expirationHourToken)
							.signWith( SignatureAlgorithm.HS512 , signKey )
							.compact();
		
		return token;
	}

	@Override
	public Claims adquireClaims(String token) throws ExpiredJwtException {
		return Jwts
				.parser()
				.setSigningKey(signKey)
				.parseClaimsJws(token)
				.getBody();
	}

	@Override
	public boolean isTokenValid(String token) {
		try {
			Claims claims = adquireClaims(token);
			java.util.Date dateEx = claims.getExpiration();
			LocalDateTime expirationDate = dateEx.toInstant()
					.atZone(ZoneId.systemDefault()).toLocalDateTime();
			boolean actualDateHourIsAfterExpirationDate = LocalDateTime.now().isAfter(expirationDate);
			return !actualDateHourIsAfterExpirationDate;
		}catch(ExpiredJwtException e) {
			return false;
		}
	}

	@Override
	public String adquireLoginUser(String token) {
		Claims claims = adquireClaims(token);
		return claims.getSubject();
	}

}
