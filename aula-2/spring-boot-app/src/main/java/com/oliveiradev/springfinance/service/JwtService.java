package com.oliveiradev.springfinance.service;

import com.oliveiradev.springfinance.model.entity.User;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;

public interface JwtService {
	
	String generateToken(User user);
	
	Claims adquireClaims(String token) throws ExpiredJwtException;
	
	boolean isTokenValid(String token);
	
	String adquireLoginUser( String token );
	
}
