package com.oliveiradev.springfinance.model.enums;

public enum AppointmentStatus {
	OPEN,
	CANCELED,
	CLOSED
}
