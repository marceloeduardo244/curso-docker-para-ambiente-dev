package com.oliveiradev.springfinance.api.dto;

import java.util.Objects;

public class TokenDTO {
	
	private String name;
	
	private String token;
	
	public TokenDTO() {
		
	}
	
	public TokenDTO(String name, String token) {
		super();
		this.name = name;
		this.token = token;
	}

	public String getName() {
		return name;
	}

	public void setName(String nome) {
		this.name = nome;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	@Override
	public int hashCode() {
		return Objects.hash(name, token);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TokenDTO other = (TokenDTO) obj;
		return Objects.equals(name, other.name) && Objects.equals(token, other.token);
	}

	@Override
	public String toString() {
		return "TokenDTO [name=" + name + ", token=" + token + "]";
	}
	
	
}
