package com.oliveiradev.springfinance.exceptions;

public class ApplicationPersonalizatedRulesException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ApplicationPersonalizatedRulesException(String msg) {
		super(msg);
	}
	
}
