package com.oliveiradev.springfinance.api.dto;

import java.math.BigDecimal;
import java.util.Objects;

import com.oliveiradev.springfinance.model.entity.User;
import com.oliveiradev.springfinance.model.enums.AppointmentStatus;
import com.oliveiradev.springfinance.model.enums.AppointmentType;

public class AppointmentDTO {

    private Long id;
    private String description;
    private Integer month;
    private Integer year;
    private BigDecimal value;
    private AppointmentType type;
    private AppointmentStatus status;
    private User user;
    
    public AppointmentDTO() {
    	
    }
    
	public AppointmentDTO(Long id, String description, Integer month, Integer year, BigDecimal value,
			AppointmentType type, AppointmentStatus status, User user) {
		super();
		this.id = id;
		this.description = description;
		this.month = month;
		this.year = year;
		this.value = value;
		this.type = type;
		this.status = status;
		this.user = user;
	}

	public AppointmentDTO(String description, Integer month, Integer year, BigDecimal value,
			AppointmentType type, AppointmentStatus status, User user) {
		super();
		this.description = description;
		this.month = month;
		this.year = year;
		this.value = value;
		this.type = type;
		this.status = status;
		this.user = user;
	}

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Integer getMonth() {
		return month;
	}
	public void setMonth(Integer month) {
		this.month = month;
	}
	public Integer getYear() {
		return year;
	}
	public void setYear(Integer year) {
		this.year = year;
	}
	public BigDecimal getValue() {
		return value;
	}
	public void setValue(BigDecimal value) {
		this.value = value;
	}
	public AppointmentType getType() {
		return type;
	}
	public void setType(AppointmentType type) {
		this.type = type;
	}
	public AppointmentStatus getStatus() {
		return status;
	}
	public void setStatus(AppointmentStatus status) {
		this.status = status;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}

	@Override
	public int hashCode() {
		return Objects.hash(description, id, month, status, type, user, value, year);
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AppointmentDTO other = (AppointmentDTO) obj;
		return Objects.equals(description, other.description) && Objects.equals(id, other.id)
				&& Objects.equals(month, other.month) && status == other.status && type == other.type
				&& Objects.equals(user, other.user) && Objects.equals(value, other.value)
				&& Objects.equals(year, other.year);
	}
	@Override
	public String toString() {
		return "AppointmentDTO [id=" + id + ", description=" + description + ", month=" + month + ", year=" + year
				+ ", value=" + value + ", type=" + type + ", status=" + status + ", user=" + user + "]";
	}
	
}
