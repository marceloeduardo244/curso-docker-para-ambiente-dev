package com.oliveiradev.springfinance.service.impl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.oliveiradev.springfinance.model.entity.User;
import com.oliveiradev.springfinance.model.repository.UserRepository;
import com.oliveiradev.springfinance.service.UserService;
import com.oliveiradev.springfinance.exceptions.ApplicationPersonalizatedRulesException;
import com.oliveiradev.springfinance.exceptions.AuthenticationErrorException;

@Service
public class UserServiceImpl implements UserService {
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private PasswordEncoder encoder;


	@Override
	public void validateEmail(String email) {
		boolean exist = userRepository.existsByEmail(email);
		
		if (exist) {
			throw new ApplicationPersonalizatedRulesException("This email alerady exists!");
		}
	}


	@Override
	public User authenticate(String email, String password) {
		Optional<User> possibleUser = userRepository.findByEmail(email);
		
		if (!possibleUser.isPresent()) {
			throw new AuthenticationErrorException("Email not found!");
		}
		
		boolean matchPassword = encoder.matches(password, possibleUser.get().getPassword());
		
		if (!matchPassword) {
			throw new AuthenticationErrorException("Invalid password!");
		}
		
		return possibleUser.get();
	}


	@Override
	@Transactional
	public User createUser(User user) {
		validateEmail(user.getEmail());
		hashPassword(user);
		return userRepository.save(user);
	}
	
	public void hashPassword(User user) {
		String hashedPassword = encoder.encode(user.getPassword());
		user.setPassword(hashedPassword);
	}


	@Override
	public Optional<User> getUserById(Long id) {
		Optional<User> possibleUser = userRepository.findById(id);
		
		if (!possibleUser.isPresent()) {
			throw new ApplicationPersonalizatedRulesException("User not found!");
		}
		
		return possibleUser;
	}


	@Override
	@Transactional
	public void deleteUser(User user) {
		userRepository.delete(user);
	}
	
	@Override
	@Transactional
	public void deleteUserByEmail(String email) {
		userRepository.deleteByEmail(email);
	}

}
