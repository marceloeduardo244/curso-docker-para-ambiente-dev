package com.oliveiradev.springfinance.model.repository;

import java.math.BigDecimal;
import java.util.Optional;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.oliveiradev.springfinance.model.entity.Appointment;
import com.oliveiradev.springfinance.model.enums.AppointmentStatus;
import com.oliveiradev.springfinance.model.enums.AppointmentType;

@SpringBootTest
@ExtendWith(SpringExtension.class)
@ActiveProfiles("test")
public class AppointmentRepositoryTest {

	@Autowired
	AppointmentRepository appointmentRepository;
	
	@Test
	public void mustSaveAnAppointment() {
		Appointment appointment = createAppointment();
		
		appointment = appointmentRepository.save(appointment);
		
		Assertions.assertTrue(appointment.getId() != null);
		
		appointmentRepository.delete(appointment);
	}
	
	@Test
	public void mustDeleteAnAppointment() {
		Appointment appointment = createAndSaveAppointment();
		
		Optional<Appointment> createdAppointment = appointmentRepository.findById(appointment.getId());
		
		if (createdAppointment.isPresent()) {
			appointmentRepository.delete(appointment);
			
			Optional<Appointment> searchedAppointment = appointmentRepository.findById(createdAppointment.get().getId());
			Assertions.assertFalse(searchedAppointment.isPresent());
		}
	}
	
	@Test
	public void mustUpdateAnAppointment() {
		Appointment appointment = createAndSaveAppointment();
		
		appointment.setYear(2018);
		appointment.setDescription("Teste Atualizar");
		appointment.setStatus(AppointmentStatus.CANCELED);
		
		appointmentRepository.save(appointment);
		
		Optional<Appointment> updatedAppointment = appointmentRepository.findById(appointment.getId());
		
		if (updatedAppointment.isPresent()) {
			Assertions.assertTrue(updatedAppointment.get().getYear().equals(2018));
			Assertions.assertTrue(updatedAppointment.get().getDescription().equals("Teste Atualizar"));
			Assertions.assertTrue(updatedAppointment.get().getStatus().equals(AppointmentStatus.CANCELED));
		}
		
		appointmentRepository.delete(appointment);
	}
	
	@Test
	public void mustFindAnAppointmentById() {
		Appointment appointment = createAndSaveAppointment();
		
		Optional<Appointment> findedAppointment = appointmentRepository.findById(appointment.getId());
		
		Assertions.assertTrue(findedAppointment.isPresent());
		
		appointmentRepository.delete(appointment);
	}
	
	public Appointment createAndSaveAppointment() {
		Appointment appointment = createAppointment();
		appointmentRepository.save(appointment);
		
		return appointment;
	}
	
	public static Appointment createAppointment() {
		Appointment appointment = new Appointment();
		
		appointment.setDescription("test_integration_587");
		appointment.setMonth(3);
		appointment.setStatus(AppointmentStatus.OPEN);
		appointment.setType(AppointmentType.DEBIT);
		appointment.setUser(null);
		appointment.setValue(BigDecimal.valueOf(25.50));
		appointment.setYear(2023);
		
		return appointment;
	}
	
}
