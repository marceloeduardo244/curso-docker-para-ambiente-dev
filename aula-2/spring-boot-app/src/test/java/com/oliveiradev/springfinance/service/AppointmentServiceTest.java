package com.oliveiradev.springfinance.service;

import static org.mockito.Mockito.doNothing;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.oliveiradev.springfinance.exceptions.ApplicationPersonalizatedRulesException;
import com.oliveiradev.springfinance.model.entity.Appointment;
import com.oliveiradev.springfinance.model.entity.User;
import com.oliveiradev.springfinance.model.enums.AppointmentStatus;
import com.oliveiradev.springfinance.model.enums.AppointmentType;
import com.oliveiradev.springfinance.model.repository.AppointmentRepository;
import com.oliveiradev.springfinance.model.repository.AppointmentRepositoryTest;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@SpringBootTest
@ExtendWith(SpringExtension.class)
@ActiveProfiles("test")
public class AppointmentServiceTest {
	
	@Autowired
	AppointmentService appointmentService;
	
	@Autowired
	UserService userService;
	
	@Autowired
	AppointmentRepository appointmentRepository;
	
    @BeforeAll
    public void init() {
    	appointmentService.deleteByDescription("test_integration_587");
    	userService.deleteUserByEmail("test_integration_587@email.com");
    }

    @AfterAll
    public void teardown() {
    	appointmentService.deleteByDescription("test_integration_587");
    	userService.deleteUserByEmail("test_integration_587@email.com");
    }
	
	public User createMockUser() {
		userService.deleteUserByEmail("test_integration_587@email.com");
		User user = new User("test_integration_587", "test_integration_587@email.com", "123456");
		userService.createUser(user);
		return user;
	}
	
	@Test
	public void mustSaveAnAppointment() {
		//cenário
		Appointment appointmentToSave = AppointmentRepositoryTest.createAppointment();
		
		User createdUser = createMockUser();
		
		appointmentToSave.setUser(createdUser);
		
		//execucao
		Appointment createdAppointment = appointmentService.createAppointment(appointmentToSave);
		
		//verificação
		Assertions.assertTrue(createdAppointment.getId().equals(appointmentToSave.getId()));
		Assertions.assertTrue(createdAppointment.getStatus().equals(appointmentToSave.getStatus()));
		
		appointmentService.deleteAppointment(createdAppointment);
		userService.deleteUser(createdUser);
	}
	
	@Test
	public void mustNotSaveAnAppointmentWhenHasValidationErrors() {
		//cenário
		Appointment appointmentToSave = AppointmentRepositoryTest.createAppointment();
		
		Assertions.assertThrows(ApplicationPersonalizatedRulesException.class, () -> {
			
			// ação
			appointmentService.validateAppointment(appointmentToSave);
		});
	}
	
	@Test
	public void mustUpdateAnAppointment() {
		//cenário
		Appointment appointmentToSave = AppointmentRepositoryTest.createAppointment();
		
		User createdUser = createMockUser();
		
		appointmentToSave.setUser(createdUser);
		
		//execucao
		Appointment createdAppointment = appointmentService.createAppointment(appointmentToSave);
		
		createdAppointment.setDescription("changed");
		
		Appointment updatedAppointment = appointmentService.updateAppointment(createdAppointment);

		Assertions.assertTrue(updatedAppointment.getDescription().equals("changed"));
		
		appointmentService.deleteAppointment(createdAppointment);
		userService.deleteUser(createdUser);
	}
	
	@Test
	public void mustThrowErrorWhenTryToUpdateAnAppointmentUnsaved() {
		//cenário
		Appointment appointmentToSave = AppointmentRepositoryTest.createAppointment();
		User createdUser = createMockUser();
		appointmentToSave.setUser(createdUser);

		Assertions.assertThrows(Exception.class, () -> {
			
			// ação
			appointmentService.updateAppointment(appointmentToSave);
		});
		
		userService.deleteUser(createdUser);
	}
	
	@Test
	public void mustDeleteAnAppointment() {
		//cenário
		Appointment appointmentToSave = AppointmentRepositoryTest.createAppointment();
		User createdUser = createMockUser();
		
		appointmentToSave.setUser(createdUser);
		
		//execucao
		Appointment createdAppointment = appointmentService.createAppointment(appointmentToSave);
		
		Assertions.assertDoesNotThrow(() -> {
			
			// ação
			appointmentService.deleteAppointment(createdAppointment);
		});
		
		userService.deleteUser(createdUser);
	}
	
	@Test
	public void mustThrowErrorWhenTryToDeleteAnAppointmentUnsaved() {
		
		//cenário
		Appointment appointmentToSave = AppointmentRepositoryTest.createAppointment();
		User createdUser = createMockUser();
		appointmentToSave.setUser(createdUser);
		
		Assertions.assertThrows(Exception.class, () -> {
			
			// ação
			appointmentService.deleteAppointment(appointmentToSave);
		});
		
		userService.deleteUser(createdUser);
	}
	
	@Test
	public void mustFilterAppointments() {
		//cenário
		Appointment appointmentToSave = AppointmentRepositoryTest.createAppointment();
		User createdUser = createMockUser();
		appointmentToSave.setUser(createdUser);
		
		//execucao
		Appointment createdAppointment = appointmentService.createAppointment(appointmentToSave);
		
		List<Appointment> listOfAppointments = appointmentService.getListAppointments(createdAppointment);
		
		//verificacoes
		Assertions.assertTrue(!listOfAppointments.isEmpty());
		
		appointmentService.deleteAppointment(createdAppointment);
		userService.deleteUser(createdUser);
	}
	
	@Test
	public void mustUpdateStatusOfAnAppointment() {
		//cenário
		Appointment appointmentToSave = AppointmentRepositoryTest.createAppointment();
		User createdUser = createMockUser();
		appointmentToSave.setUser(createdUser);
		
		//execucao
		Appointment createdAppointment = appointmentService.createAppointment(appointmentToSave);
		
		appointmentService.updateStatus(createdAppointment, AppointmentStatus.CLOSED);
		
		Optional<Appointment> updatedAppointment = appointmentService.getAppointmentById(createdAppointment.getId());
		
		if (updatedAppointment.isPresent()) {
			Assertions.assertTrue(updatedAppointment.get().getStatus().equals(AppointmentStatus.CLOSED));
		}
		
		appointmentService.deleteAppointment(createdAppointment);
		userService.deleteUser(createdUser);
	}
	
	@Test
	public void mustGetAnAppointmentById() {
		//cenário
		Appointment appointmentToSave = AppointmentRepositoryTest.createAppointment();
		User createdUser = createMockUser();
		appointmentToSave.setUser(createdUser);
		
		//execucao
		Appointment createdAppointment = appointmentService.createAppointment(appointmentToSave);
		
		Optional<Appointment> searchedAppointment = appointmentService.getAppointmentById(createdAppointment.getId());
		
		Assertions.assertTrue(searchedAppointment.isPresent());
		
		appointmentService.deleteAppointment(createdAppointment);
		userService.deleteUser(createdUser);
	}
	
	@Test
	public void mustNotGetAnAppointmentById() {
		//cenário
		Appointment appointmentToSave = AppointmentRepositoryTest.createAppointment();
		User createdUser = createMockUser();
		appointmentToSave.setUser(createdUser);
		
		//execucao
		Appointment createdAppointment = appointmentService.createAppointment(appointmentToSave);
		
		Long id = createdAppointment.getId();
		
		appointmentService.deleteAppointment(createdAppointment);
		userService.deleteUser(createdUser);
		
		Optional<Appointment> searchedAppointment = appointmentService.getAppointmentById(id);
		
		Assertions.assertFalse(searchedAppointment.isPresent());
	}
	
	@Test
	public void mustGetBalanceByUser() {
		//cenário
		Appointment appointmentToSave = AppointmentRepositoryTest.createAppointment();
		User createdUser = createMockUser();
		appointmentToSave.setUser(createdUser);
		appointmentToSave.setValue(BigDecimal.valueOf(100));
		appointmentToSave.setType(AppointmentType.DEBIT);
		
		//execucao
		Appointment createdAppointment1 = appointmentService.createAppointment(appointmentToSave);
		appointmentService.updateStatus(createdAppointment1, AppointmentStatus.CLOSED);
		
		Appointment appointmentToSave2 = AppointmentRepositoryTest.createAppointment();
		appointmentToSave2.setUser(createdUser);
		appointmentToSave2.setValue(BigDecimal.valueOf(200));
		appointmentToSave2.setType(AppointmentType.RECEIPT);
		
		//execucao
		Appointment createdAppointment2 = appointmentService.createAppointment(appointmentToSave2);
		appointmentService.updateStatus(createdAppointment2, AppointmentStatus.CLOSED);
		
		BigDecimal balance = appointmentService.getBalanceByUserId(createdUser.getId());

		Assertions.assertTrue(balance.intValue() == 100);
		
		appointmentService.deleteAppointment(createdAppointment1);
		appointmentService.deleteAppointment(createdAppointment2);
		userService.deleteUser(createdUser);
	}

}
