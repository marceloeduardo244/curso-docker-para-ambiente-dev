package com.oliveiradev.springfinance.service;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.oliveiradev.springfinance.exceptions.ApplicationPersonalizatedRulesException;
import com.oliveiradev.springfinance.exceptions.AuthenticationErrorException;
import com.oliveiradev.springfinance.model.entity.User;
import com.oliveiradev.springfinance.model.repository.UserRepository;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@SpringBootTest
@ExtendWith(SpringExtension.class)
@ActiveProfiles("test")
public class UserServiceTest {
	
	@Autowired
	UserService userService;
	
	@Autowired
	UserRepository repository;
	
	@Autowired
	AppointmentService appointmentService;
	
    @BeforeAll
    public void init() {
    	appointmentService.deleteByDescription("test_integration_587");
    	userService.deleteUserByEmail("test_integration_587@email.com");
    }

    @AfterAll
    public void teardown() {
    	appointmentService.deleteByDescription("test_integration_587");
    	userService.deleteUserByEmail("test_integration_587@email.com");
    }
	
	public User createMockUser() {
		userService.deleteUserByEmail("test_integration_587@email.com");
		User user = new User("test_integration_587", "test_integration_587@email.com", "123456");
		return user;
	}
	
	@Test
	public void mustCreateNewUser() {
		User user = createMockUser();
		User userCreated = userService.createUser(user);
		
		Assertions.assertTrue(userCreated.getId() != null);
		
		Assertions.assertTrue(userCreated.getId().equals(userCreated.getId()));
		Assertions.assertTrue(userCreated.getEmail().equals(userCreated.getEmail()));
		Assertions.assertTrue(userCreated.getName().equals(userCreated.getName()));
		Assertions.assertTrue(userCreated.getPassword().equals(userCreated.getPassword()));
		
		userService.deleteUser(user);
	}
	
	@Test
	public void mustValidateEmail() {
		Assertions.assertDoesNotThrow(() -> {
			// ação
			userService.validateEmail("test_integration_587_new@email.com");
		});
	}
	
	@Test
	public void mustNotValidateWithEmail() {
		// cenário
		User user = createMockUser();
		userService.createUser(user);
		
		Assertions.assertThrows(ApplicationPersonalizatedRulesException.class, () -> {
			
			// ação
			userService.validateEmail("test_integration_587@email.com");
		});
		
		userService.deleteUser(user);
	}
	
	@Test
	public void mustAuthenticate() {
		User user = createMockUser();
		userService.createUser(user);
		
		Assertions.assertDoesNotThrow(() -> {
			// ação
			userService.authenticate(user.getEmail(), "123456");
		});
		
		userService.deleteUser(user);
	}
	
	@Test
	public void mustNotAuthenticatePasswordInvalid() {
		// cenário
		User user = createMockUser();
		userService.createUser(user);
		
		Assertions.assertThrows(AuthenticationErrorException.class, () -> {
			
			// ação
			userService.authenticate(user.getEmail(), "invalid_password");
		});
		
		userService.deleteUser(user);
	}
	
	@Test
	public void mustNotAuthenticateEmailNotFound() {
		// cenário
		User user = createMockUser();
		userService.createUser(user);
		
		Assertions.assertThrows(AuthenticationErrorException.class, () -> {
			
			// ação
			userService.authenticate("invalid_email@invalid.com", user.getPassword());
		});
		
		userService.deleteUser(user);
	}
	
	@Test
	public void mustFoundUserById() {
		User user = createMockUser();
		userService.createUser(user);
		
		Assertions.assertDoesNotThrow(() -> {
			// ação
			userService.getUserById(user.getId());
		});
		
		userService.deleteUser(user);
	}
	
	@Test
	public void mustNotFoundUserById() {
		// cenário
		User user = createMockUser();
		userService.createUser(user);
		userService.deleteUser(user);
		
		Assertions.assertThrows(ApplicationPersonalizatedRulesException.class, () -> {
			// ação
			userService.getUserById(user.getId());
		});
	}

}
